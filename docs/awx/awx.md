# Monaco Telecom Rancher Docs - Ansible AWX

********************************************************************
## Install Ansible AWX on Rancher Server
********************************************************************
[Ansible AWX](https://github.com/ansible/awx) is an Open Source version of Ansible Tower and licensed under Apache v2.
Ansible AWX is one of the upstream project for Red Hat Ansible Tower. AWX provides a web-based user interface, REST API, and task engine built on top of Ansible.

In this tutorial, AWX will be installed using [AWX Operator](https://github.com/ansible/awx-operator) on top of RKE cluster managed by Rancher Server.

This tutorial walks you through:

- Configure the Kubectl
- Clone the AWX Operator project
- Create Kubernetes namespace for AWX
- Build and Install the AWX Operator with Kustomize
- Creating Kubernetes Ressources (peristent volume PV, persistent claim volumes PVC, TLS secret for https access)
- Install AWX stack (Postgresql and AWX deployment)

This tutorial is divided into different tasks for easier consumption.

1. [Requirements](#1-requirements)

1. [Install AWX Kubernetes Operator](#2-install-awx-kubernetes-operator)

1. [Install AWX Stack](#3-install-awx-stack)

1. [Accessing the AWX User Interface](#4-accessing-the-awx-user-interface)

1. [Check AWX k8s Resources with Rancher UI](#5-check-awx-k8s-resources-with-rancher-ui)

1. [Setup Custom AWX Execution Environment](#6-setup-custom-awx-execution-environment)

1. [Create a new AWX Execution Environement with the AWX EE Image](#7-create-a-new-awx-execution-environement-with-the-awx-ee-image)

1. [Setup AWX with Gitlab](#8-setup-awx-with-gitlab)

1. [Create Your First Project - Demo](#9-create-your-first-project-demo)

1. [Create Your First Openstack Tenant With AWX](#10-create-your-first-openstack-tenant-with-awx)


<br/>

### 1. Requirements

To run the AWX stack, we need below minimum system requirements :

- Minimum of 6GB of memory
- At least 3 CPU cores
- Minimum 20GB of disk space
- Running Kubernetes Cluster

We will customize ressources ans storage requirements in the upcoming steps.

To begin with, the Ansible AWX installation on Kubernetes, Make sure to have an up and running Kubernetes cluster.

```
$  cat ~/.kube/config
apiVersion: v1
clusters:
- cluster:ertificate-authority-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUdhakxxxxxxxxxxxxxxxxxxxxxxxxxxx
server: xxxxxxxxxx
  name: xxx
contexts:
- context:
    cluster: xxx
    namespace: xx
    user: xx
  name: xx
current-context: xx
kind: Config
preferences: {}
users:
- name: xx
  user:
    token: xxxxxx
$ kubectl get nodes
NAME            STATUS   ROLES               AGE     VERSION
rke-master-01   Ready    controlplane,etcd   2d18h   v1.21.7
rke-master-02   Ready    controlplane,etcd   2d18h   v1.21.7
rke-master-03   Ready    controlplane,etcd   2d18h   v1.21.7
rke-worker-01   Ready    worker              2d18h   v1.21.7
rke-worker-02   Ready    worker              2d18h   v1.21.7
rke-worker-03   Ready    worker              2d18h   v1.21.7

```

### 2. Install AWX Kubernetes Operator

#### Clone from Github the AWX Operator Project

```
$ git clone https://github.com/ansible/awx-operator.git
$ cd awx-operator

```

#### Checkout the last version of AWX Operator

Execute the below commands inside the `awx-operator` folder.

```
$ export RELEASE_TAG=`curl -s https://api.github.com/repos/ansible/awx-operator/releases/latest | grep tag_name | cut -d '"' -f 4`
$ git checkout $RELEASE_TAG

```

#### Create Namespace for AWX

```
$ export NAMESPACE=awx
$ kubectl create ns ${NAMESPACE}
$ kubectl config set-context --current --namespace=$NAMESPACE
$ kubectl get po
```

#### Deploy the AWX Operator

The `make deploy` command will: 

- Download kustomize locally if necessary.
- Using kustomize to patch the awx operator image in the `config/manager/kustomization.yaml ` file
- Using kustomize to build k8s manifests in the `config/defaut`
- Using the kubectl to deploy all the CRD and the AWX controller (CRDs, service accounts, roles, clusterroles, etc) to the K8s cluster specified in ~/.kube/config.

**Note:**
You can check the `##@ Deployment` section in the `Makefile` file.

Execute the below commands inside the `awx-operator` folder.

```
$ make deploy
```
Wait a bit and you should have the awx-operator running:

```
$ ls config/default
$ kubectl  get po
NAME                                              READY   STATUS    RESTARTS   AGE
awx-operator-controller-manager-b775bfc7c-5f7hr   2/2     Running   0          50s

```

It is this pod that is responsible for installing AWX from a configuration that we will indicate to it later.

### 3. Install AWX Stack

#### Creating AWX Deployment

Create a file named `deploy-awx.yml` in the `config/default` folder with the suggested content below. The `metadata.name` you provide will be the name of the resulting AWX deployment.

**Note:**
If you deploy more than one AWX instance to the same namespace, be sure to use unique names.
Once AWX is deployed, 2 pods will be created : Postegrsql (with one conainer), and AWX pod (with 4 containers respectively named web, task, execution and redis).

Create a file and append the YAML content:

```
$ cat deploy-awx.yml
---
apiVersion: awx.ansible.com/v1beta1
kind: AWX
metadata:
  name: awx
spec:
  web_resource_requirements:
    requests:
      cpu: 250m
      memory: 128M
  task_resource_requirements:
    requests:
      cpu: 250m
      memory: 512M
  ee_resource_requirements:
    requests:
      cpu: 200m
      memory: 512M
  postgres_resource_requirements:
    requests:
      cpu: 500m
      memory: 1Gi
    limits:
      cpu: 1
      memory: 1Gi
  ingress_type: ingress
  ingress_path: /
  ingress_path_type: ImplementationSpecific
  hostname: awx.prod.lan
  ingress_tls_secret: tls-awx-ingress
  projects_persistence: true
  projects_storage_access_mode: ReadWriteOnce
  web_extra_volume_mounts: |
    - name: awx-data
      mountPath: /var/lib/projects
  extra_volumes: |
    - name: awx-data
      persistentVolumeClaim:
        claimName: awx-pvc


```
2. Containers Resource Requirements :
  The resource requirements for the task, the execution and the web containers are configurable. The lower end (requests) and the upper end (limits). You can check the default containers resource requirements for both [AWX](https://github.com/ansible/awx-operator#containers-resource-requirements) pod and [Postegrsql](https://github.com/ansible/awx-operator#managed-postgresql-service) pod.
3. Persisting Projects Directory :
  As part of AWX operator, three Persistent Volume Claim will be created (one PVC for the `postegrsql pod` and two PVC for AWX pod, respectively  for AWX web container and AWX Task & Execution containers). PVCs will be in a pending state if we don’t have three availble Persistent Volume.
  By default the `postegrsql pod` is persistent (default is `/var/lib/postgresql/data/pgdata)`. We find [here](https://github.com/ansible/awx-operator#managed-postgresql-service) all   variables to  be customized for PostgreSQL service.
  In cases which you want to persist the `/var/lib/projects directory`, there are few variables that are customizable for the awx-operator :
      - Set `projects_persistence` to `true`. This parameter shows whether or not the /var/lib/projects directory will be persistent. **Important** You should create three PV before deploying AWX if storageclass is not defined by default  in your cluster (see next section). 
      - Set `projects_storage_access_mode` to `ReadWriteOnce`
      - **Optional** Define  the  the PersistentVolume storage class (your k8s StorageClass by default) by setting the `projects_storage_class` parameter. However, in this tutorial we don't have a default storage class, so we will provision manually all persistent volumes in the next step.
      - If you have already created your PVC you can customize Volume and Volume Mount Options [here](https://github.com/ansible/awx-operator#persisting-projects-directory). For demonstration, in this tutorial we create a PVC only for the AWX web container (with the `web_extra_volume_mounts` parameter), and the AWX Operator will automatically  provision the other PVC (see next section). 

4. Ingress : 
  By default, the AWX operator is not opinionated and won't force a specific ingress type on you. So, when the `ingress_type` is not specified, it will default to `none` and nothing ingress-wise will be created. All information about Ingress are availbale [here](https://github.com/ansible/awx-operator#ingress-type).
  In this tutorial, we set the `ingress_type` parameter to `ingress` and TLS. The ingress type creates an Ingress resource  which can be shared with the Nginx Ingress Controllers or others. 

- Service Type : If the service_type is not specified, the ClusterIP service will be used for your AWX Tower service.

#### Creating Persistent Volume

Let's create three Persistent Volume respectively  for both AWX  and Postegrsql pods.

- Option 1 By `kubectl`

```
$ cd config/default
$ cat pv-awx-1.yml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-awx-1
spec:
  accessModes:
  - ReadWriteOnce
  capacity:
    storage: 8Gi
  hostPath:
    path: /opt/pv-awx-1
    type: DirectoryOrCreate
  persistentVolumeReclaimPolicy: Retain
  volumeMode: Filesystem

$ kubectl apply -f pv-awx-1.yml

$ cat pv-awx-2.yml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-awx-2
spec:
  accessModes:
  - ReadWriteOnce
  capacity:
    storage: 8Gi
  hostPath:
    path: /opt/pv-awx-2
    type: DirectoryOrCreate
  persistentVolumeReclaimPolicy: Retain
  volumeMode: Filesystem

$ kubectl apply -f pv-awx-2.yml

$ cat pv-awx-3.yml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-awx-3
spec:
  accessModes:
  - ReadWriteOnce
  capacity:
    storage: 8Gi
  hostPath:
    path: /opt/pv-awx-3
    type: DirectoryOrCreate
  persistentVolumeReclaimPolicy: Retain
  volumeMode: Filesystem

$ kubectl apply -f pv-awx-3.yml

$ kubectl get pv
```

- Option 2 With Rancher Server UI

![Rancher PV](./img/awx/pv.PNG)

#### Creating Persistent Claim Volume (only for AWX web container)

- As mentioned in the previous section, AWX needs three PVC to persist projects data and database. In the `deploy-awx.ym` file,  the variable `extra_volumes` is already  set to `awx-pvc` value. So, we need to create with kubectl the `awx-pvc` PVC.

- Create manually the first PVC :

```
$ cd config/default
$ cat pvc-awx.yml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: awx-pvc
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 8Gi
$ kubectl apply -f pvc-awx.yml
```

- AWX controller will provision two other PVC for Postegrsql container and AWX Task & Execution containers.

#### Creating TLS secret For Ingress

- Copy/Paste your Key and Certificate in the `awx-ingress-tls-certificates/` folder :

```
$ ls config/default/awx-ingress-tls-certificates/
tls.crt  tls.key
$ cd awx-ingress-tls-certificates/
$ kubectl create secret tls tls-awx-ingress --cert=tls.crt --key=tls.key
$ kubectl describe secret tls-awx-ingress

```

#### Execute AWX Deployment

- Apply the Kubectl command  

```
$ cd config/default/
$ kubectl apply -f  deploy-awx.yml

```
- If everything goes well, we should get the below result. These are the pods, deployments and services will be up and running.



```
$ kubectl get po
NAME                                              READY   STATUS    RESTARTS   AGE
awx-64f7f7b7b8-8xzth                              4/4     Running   0          1h
awx-operator-controller-manager-b775bfc7c-5f7hr   2/2     Running   0          2h
awx-postgres-0                                    1/1     Running   0          1h

$ kubectl get pods -l "app.kubernetes.io/managed-by=awx-operator"
NAME                   READY   STATUS    RESTARTS   AGE
awx-64f7f7b7b8-8xzth   4/4     Running   0          1h
awx-postgres-0         1/1     Running   0          1h

$ kubectl get svc -l "app.kubernetes.io/managed-by=awx-operator"
NAME           TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
awx-postgres   ClusterIP   None            <none>        5432/TCP   1h
awx-service    ClusterIP   10.43.154.226   <none>        80/TCP     1h


```

- Check PV and PVC

```
$ kubectl get pv
kNAME       CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM                         STORAGECLASS   REASON   AGE
pv-awx-1   8Gi        RWO            Retain           Bound    awx/postgres-awx-postgres-0                           1h
pv-awx-2   8Gi        RWO            Retain           Bound    awx/awx-projects-claim                                1h
pv-awx-3   8Gi        RWO            Retain           Bound    awx/awx-pvc                                           1h

$ kubectl get pvc
NAME                      STATUS   VOLUME     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
awx-projects-claim        Bound    pv-awx-2   8Gi        RWO                           1h
awx-pvc                   Bound    pv-awx-3   8Gi        RWO                           1h
postgres-awx-postgres-0   Bound    pv-awx-1   8Gi        RWO                           1h

```

- Check Ingress

```
$ kubectl  get ingress
NAME          CLASS    HOSTS          ADDRESS                                  PORTS     AGE
awx-ingress   <none>   awx.prod.lan   192.168.1.31,192.168.1.32,192.168.1.33   80, 443   24h

$ kubectl  describe ingress awx-ingress
Name:             awx-ingress
Labels:           app.kubernetes.io/component=awx
                  app.kubernetes.io/managed-by=awx-operator
                  app.kubernetes.io/name=awx
                  app.kubernetes.io/operator-version=0.19.0
                  app.kubernetes.io/part-of=awx
Namespace:        awx
Address:          192.168.1.31,192.168.1.32,192.168.1.33
Default backend:  default-http-backend:80 (<error: endpoints "default-http-backend" not found>)
TLS:
  tls-awx-ingress terminates awx.prod.lan
Rules:
  Host          Path  Backends
  ----          ----  --------
  awx.prod.lan
                /   awx-service:80 (10.42.228.75:8052)
Annotations:    field.cattle.io/publicEndpoints:
                  [{"addresses":["192.168.1.31","192.168.1.32","192.168.1.33"],"port":443,"protocol":"HTTPS","serviceName":"awx:awx-service","ingressName":"...
Events:         <none>
```

### 4. Accessing the AWX User Interface
Now, it’s time to access the web interface.

- Log in to your AWX server with your browser via `https://awx.prod.lan/`
```
$ kubectl get secret awx-admin-password -o go-template='{{range $k,$v := .data}}{{printf "%s: " $k}}{{if not $v}}{{$v}}{{else}}{{$v | base64decode}}{{end}}{{"\n"}}{{end}}'
password: J9saxdxxxxxxxxxxxxxxxxx

```

![AWX Login](./img/awx/Capture1.PNG)


- Right after login screen we will get the dashboard.


![AWX Dash](./img/awx/Capture2.PNG)


### 5. Check AWX k8s Resources with Rancher UI

- Deployment

![AWX Dash](./img/awx/Capture3.PNG)

- Pods

![AWX Dash](./img/awx/Capture4.PNG)

- Containers

![AWX Dash](./img/awx/Capture5.PNG)


- Services

![AWX Dash](./img/awx/Capture6.PNG)


- Ingress

![AWX Dash](./img/awx/Capture7.PNG)

- Persistent Volume

![AWX Dash](./img/awx/Capture8.PNG)

- Persistent Volume Claim

![AWX Dash](./img/awx/Capture9.PNG)


**Result:** AWX is installed.


### 6. Setup Custom AWX Execution Environment

This section is a guide to creating and consuming a custom Execution Environment (EE) in AWX.
Execution Environments are container images that serve as Ansible control nodes. Starting in version 2.0, ansible-runner can make use of these images.
Execution Environments are meant to be a consistent, reproducible, portable, and sharable method to run Ansible Automation jobs in the exact same way on your laptop as they are executed in Ansible AWX.
See the [documentation](https://ansible-runner.readthedocs.io/en/latest/execution_environments/) for more detais.

An Execution Environment is expected to contain:

- Ansible
- Ansible Runner
- Ansible Collections
- Python and/or system dependencies of:
      -  modules/plugins in collections
      -  content in ansible-base
      -  custom user needs

#### Setup Docker
```
$ curl https://releases.rancher.com/install-docker/20.10.sh | sh
$ sudo usermod -aG docker $USER && sudo chmod 777 /var/run/docker.sock
```
#### Setup Python
```
$ sudo apt install python
$ sudo apt install python-dev
$ sudo apt install python3.8-venv

```
#### Setup Ansible Builder

- Create a new clean python venv:
```
$ mkdir ~/ansible-builder 
$ cd ~/ansible-builder
$ python3 -m venv builder
$ source builder/bin/activate
$ pip install ansible
$ pip install ansible-builder
```
#### Create the config
Clone AWX EE Config directory form Gitlab :

```
$ git clone git@gitlab.steelhome.internal:mtmc/projects/it-infrastructure/kaas.git
$ cd ansible-awx-ee
$ ls
```
**- Execution Environment Definition :**

The `execution-environment.yml` file shows an example execution environment definition schema as follows:
```
$ cat execution-environment.yml
---
version: 1
build_arg_defaults:
  EE_BASE_IMAGE: 'quay.io/ansible/ansible-runner:stable-2.12-devel'
ansible_config: 'ansible.cfg'
dependencies:
  galaxy: _build/requirements.yml
  python:
  system: _build/bindep.txt
additional_build_steps:
  append:
    - RUN alternatives --set python /usr/bin/python3
    - COPY --from=quay.io/ansible/receptor:devel /usr/bin/receptor /usr/bin/receptor
    - RUN mkdir -p /var/run/receptor
    - ADD run.sh /run.sh
    - CMD /run.sh
    - RUN python3 -m venv /var/lib/awx/venv/ansible2_9
    - RUN python3 -m venv /var/lib/awx/venv/ansible2_10
    - RUN /var/lib/awx/venv/ansible2_9/bin/pip install --upgrade pip
    - RUN /var/lib/awx/venv/ansible2_10/bin/pip install --upgrade pip
    - RUN /var/lib/awx/venv/ansible2_9/bin/pip install psutil
    - RUN /var/lib/awx/venv/ansible2_10/bin/pip install psutil
    - RUN /var/lib/awx/venv/ansible2_9/bin/pip install -U "ansible == 2.9"
    - RUN /var/lib/awx/venv/ansible2_10/bin/pip install -U "ansible == 2.10"
    - RUN /var/lib/awx/venv/ansible2_9/bin/pip install -U "python-openstackclient == 5.6.0"
    - RUN /var/lib/awx/venv/ansible2_10/bin/pip install -U "python-openstackclient == 5.6.0"
    - RUN /var/lib/awx/venv/ansible2_9/bin/pip install -U "openstackclient == 4.0.0"
    - RUN /var/lib/awx/venv/ansible2_10/bin/pip install -U "openstackclient == 4.0.0"
    - USER 1000
    - RUN git lfs install
```

**- Build Args and Base Image:**

Default values for build args can be specified in the definition file in the `build_arg_defaults` section as a dictionary. This is an alternative to using the `--build-arg` CLI flag.

All Build args used by ansible-builder are presented [here](https://ansible-builder.readthedocs.io/en/latest/definition/#).

In this guide we used only The `EE_BASE_IMAGE` build arg. This arg specifies the parent image for the execution environment (`quay.io/ansible/ansible-runner:stable-2.12-devel`).


**Note:** If the same variable is specified in the CLI `--build-arg` flag, the CLI value will take higher precedence.

**- Ansible Config File Path**

If you want to use a custom `ansible.cfg` file you can pass it to the builder by using the `ansible_config` variable in the `execution-environment.yml` file (see above).

```
$ cat ansible.cfg
[defaults]
host_key_checking = False
record_host_keys = False
retry_files_enabled = False
command_warnings = True
deprecation_warnings = True
system_warnings = True
error_on_undefined_vars = True
force_color = 1
ansible_managed = Monaco Telecom Ansible Managed File
inventory =
#roles_path = roles/

remote_tmp = /tmp/.ansible-${USER}/tmp
ansible_python_interpreter="/usr/bin/env python"
#vault_password_file = ~/ansible/.psih_vault_password_file
# Utilise pour decoder les secrets vault chiffres avec v1.2. La v1.2 permet la notion de vault-id pour permettre des passphrases differentes pour les variables.
#vault_identity_list = psih@~/ansible/.psih_vault_password_file
[ssh_connection]
ssh_args = -o ControlMaster=auto -o ControlPersist=60s -o ForwardAgent=yes -o PasswordAuthentication=no -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no


```

**- Ansible Galaxy Dependencies**

The galaxy entry points to a valid requirements file for the `ansible-galaxy` collection `install -r ...` command.

In the `execution-environment.yml` file, the entry `requirements.yml` may be a relative path from the directory of the execution environment definition’s folder, or an absolute path (for example:  `galaxy: _build/requirements.yml`)

```
$ cat _build/requirements.yml
---
collections:
  - name: awx.awx
  - name: theforeman.foreman
  - name: openstack.cloud
  - name: ovirt.ovirt
  - name: kubernetes.core
  - name: ansible.posix
  - name: ansible.windows
  - name: redhatinsights.insights
  - name: community.general
  - name: ansible.netcommon


```
**- System-level Dependencies**

The system entry points to a bindep requirements file. This will be processed by bindep and then passed to dnf, other platforms are not yet supported.

```
$ cat _build/bindep.txt
python38-devel [platform:rpm compile]
subversion [platform:rpm]
subversion [platform:dpkg]
git-lfs [platform:rpm]
gcc
python3-devel
python3-netaddr
bind-utils
nmap



```


**- Python Dependencies**

The python entry points to a valid requirements file for the `pip install -r ...` command.

The entry `requirements.txt` may be a relative path from the directory of the execution environment definition’s folder, or an absolute path.

```
$ cat requirements.txt
git+https://github.com/ansible/ansible-builder.git@devel#egg=ansible-builder
```

**- Additional Custom Build Steps**

Additional commands may be specified in the `additional_build_steps` section, either for before the main build steps (prepend) or after (append). The syntax needs to be one of the following:

  - a list (as shown via the `append` above)
  - a multi-line string (vi the `prepend` section above)



#### Build the AWX EE Image

```
$ ansible-builder build --tag=mt-awx-ee --verbosity 2 --container-runtime docker --context .
File ./_build/requirements.yml was placed in build context by user, leaving unmodified.
File ./_build/bindep.txt was placed in build context by user, leaving unmodified.
File ./_build/ansible.cfg updated time increased and will be rewritten
Running command:
  docker build -f ./Dockerfile -t mt-awx-ee .
Complete! The build context can be found at: /home/w.saad/ansible-awx-ee

$ docker images
REPOSITORY                        TAG                 IMAGE ID       CREATED          SIZE
mt-awx-ee                         latest              c2c6b1feabda   13 seconds ago   1.44GB
<none>                            <none>              93af8990704a   4 minutes ago    1.14GB
<none>                            <none>              98fcfa94a816   8 minutes ago    793MB
quay.io/ansible/ansible-runner    stable-2.12-devel   eff1e54921bc   16 hours ago     770MB
quay.io/ansible/receptor          devel               51f50eb53b42   3 days ago       258MB
quay.io/ansible/ansible-builder   latest              b0348faa7f41   6 weeks ago      755MB
```

**- Execute the AWX EE Image**

```
$ docker run -dt --name awx-ee mt-awx-ee
05c203eea0d0e7dfc68fbb9a6e8a9bb74d1492f117a3463534f064809b2ec468

$ docker ps
CONTAINER ID   IMAGE       COMMAND                  CREATED         STATUS        PORTS     NAMES
05c203eea0d0   mt-awx-ee   "entrypoint /bin/sh …"   3 seconds ago   Up 1 second             awx-ee
```

**- Check Ansible Galaxy Dependencies**
```
$ docker exec -it awx-ee bash

bash-4.4$ ls /usr/share/ansible/collections/ansible_collections/
ansible  awx  community  kubernetes  openstack  ovirt  redhatinsights  theforeman
bash-4.4$ exit
```
**- Check Python Dependencies**

```
$ docker exec -it awx-ee bash

bash-4.4$ ls /usr/local/lib/python3.8/site-packages/openstack
openstack/                     openstacksdk-0.61.0.dist-info/

bash-4.4$ ls /usr/local/lib/python3.8/site-packages/openstack

bash-4.4$ exit
```
**- Check Virutal Env**
```
$ docker exec -it awx-ee bash

bash-4.4$ ls /var/lib/awx/venv/ansible2_
ansible2_10/  ansible2_9/

bash-4.4$ ls /var/lib/awx/venv/ansible2_9
bin  include  lib  lib64  pyvenv.cfg  share

bash-4.4$ source /var/lib/awx/venv/ansible2_9/bin/activate

(ansible2_9) bash-4.4$ ansible --version
ansible 2.9.0
  config file = None
  configured module search path = ['/home/runner/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /var/lib/awx/venv/ansible2_9/lib64/python3.8/site-packages/ansible
  executable location = /var/lib/awx/venv/ansible2_9/bin/ansible
  python version = 3.8.12 (default, Sep 21 2021, 00:10:52) [GCC 8.5.0 20210514 (Red Hat 8.5.0-3)]

(ansible2_9) bash-4.4$ openstack --version
openstack 5.6.0
(ansible2_9) bash-4.4$ exit

bash-4.4$ source /var/lib/awx/venv/ansible2_10/bin/activate

(ansible2_10) bash-4.4$ ansible --version
ansible [core 2.12.4.post0]
  config file = None
  configured module search path = ['/home/runner/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /usr/local/lib/python3.8/site-packages/ansible
  ansible collection location = /home/runner/.ansible/collections:/usr/share/ansible/collections
  executable location = /usr/local/bin/ansible
  python version = 3.8.12 (default, Sep 21 2021, 00:10:52) [GCC 8.5.0 20210514 (Red Hat 8.5.0-3)]
  jinja version = 2.10.3
  libyaml = True

bash-4.4$ exit
```
**- System-level Dependencies**

```
$ docker exec -it awx-ee bash

bash-4.4$ gcc
gcc         gcc-ar      gcc-nm      gcc-ranlib

bash-4.4$ bind

bash-4.4$ nmap
Nmap 7.70 ( https://nmap.org )
Usage: nmap [Scan Type(s)] [Options] {target specification}


```

### 7. Create a new AWX Execution Environement with the AWX EE Image

#### Add a new Container Registry Credentials
- Click on **`Resources -> Credentials`**.
- What you'll need here :
    - `Credential Name`: mt-registry-internal (for ex.)
    - `AWX Organization`: Default (for ex.)
    - `Credential Type`: Container Registry
    - `Authentication URL` : docker-infra-local.jfrog-artifactory.tools.it-factory.prod.lan (for ex.)
    - `Username`: mtmc-xxxx
    - `Password or Token`: APCFFxxxxxxxxxxx


![CR Cred 1](./img/awx/cred-cr1.png)

- Check the Container Registry Credential

![CR Cred 2](./img/awx/cred-cr2.PNG)

#### Add a new AWX EE
- Click on **`Administration -> Execution Environments`**.
- What you'll need here :
    - `ENV Name` : MT-AWX-EE (for ex.)
    - `Image`: docker-infra-local.jfrog-artifactory.tools.it-factory.prod.lan/awx/mt-awx-ee:latest
    - `Organization`: Default (for ex.)
    - `Credential`: mt-registry-internal (created above)

**Note:** The `Image` should be the full image location, including the container registry, image name, and version tag.


![AWX EE 2](./img/awx/awx-ee1.png)

- Check the new AWX EE

![AWX EE 2](./img/awx/awx-ee2.png)


### 8. Setup AWX with Gitlab
This section focuses on how to setup AWX to run some jobs, using Gitlab as a source of files (playbooks, inventory and so on).

#### Add a Source Control credential for Gitlab in AWX

- Display and copy the private key:

```
 $ cat ~/.ssh/id_rsa
-----BEGIN OPENSSH PRIVATE KEY-----
xxxxxxxxxxxxxAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAABFwAAAAdzc2gtcn
NhAAAAAwEAAQAAAQEAyff5tAx1iYQXCXqd7e1WFlhCl/JXv/oJaMm7AqEcUdKCd4uBHIrj
xxxxxxxxxxxxxxxx
-----END OPENSSH PRIVATE KEY-----
```
- In AWX, Go to **`Resources, Credentials`** and click on the "Add" button to add a new credential entry. Fill in the following fields:
    - `Name`: MT Gitlab
    - `Credential Type`: Source Control
    - `SCM Private Key`: The content of the `~/.ssh/id_rsa` file
    - `Private Key Passphrase` (Optional): Your passphrase if required

![AWX Gitlab 1](./img/awx/awx-git1.PNG)

- List Your Credentials

![AWX Gitlab 2](./img/awx/awx-git2.PNG)

 

### 9. Create Your First Project - Demo

#### Create a new Project in AWX
- A `project` needs to belong to an Organization so you will first have to create one from the Access/Organization tab. In this task will use the `Default` Organization.
- Go on **`Resources -> Projects`** to add a new project. Fill in the following fields:
    - `Name`: I strongly recommanded to include the name of the branch in the project name. In case you create different projects from the same repository, this will be useful to differenciate your projects. (For ex. `MyFirstProject-dev`).
    - `Organization`: This should be defaulted to the one and only organization you have (for ex. `Default`).
    - `Execution Environment`: The execution environment that will be used for jobs that use this project.Click on search button and select the `MT-AWX-EE` execution environment (created in the previous section) and 
    - `Source Control Credential Type`: Git or Subversion (Select `Git` for this demo)
    - `Source Control URL`: Copy the SCM URL of your ansible playbooks using the Clone button in Gitlab on your repository page (the label is clone from SSH for private repositories, for ex. `git@gitlab.steelhome.internal:user/your-repo.git`).
    - `Source Control Branch/Tag/Commit`: Enter the Git branch (For ex. the `dev` branch.
    - `Source Control Credential`: Click on the search button and look for the `MT Gitlab` credential created in the step above.

**Note:** If you want AWX to fetch the latest version of the branch every time a job uses this project, select `Update Revision on Launch`. Without this option, you will have to manually refresh the project everytime you want to apply new changes from the repo.

![AWX Project 1](./img/awx/awx-project1.PNG)

- List Your Projects

![AWX Project 1](./img/awx/awx-project2.PNG)

#### Create a new Template from a Project
- Templates in AWX are used to defined the execution context of one or more playbooks.
- With AWX we can create two types of templates:
    - `Job Template` a template that refers to a **single playbook**.
    - `Workflow Template` a template that composes with **multiple playbooks**.

- Click on **`Resources -> Templates`** ad create a new Job template. 

    - `Name`: the name of the template.
    - `Job Type`: For job templates, select `run` to execute the playbook. Select `check` to only check playbook syntax, test environment setup, and report problems without executing the playbook.
    - `Inventory`: Select the inventory containing the hosts you want this job to manage.
    - `Project`: Select the project containing the playbook you want this job to execute.
    - `Execution Environment`: Select the execution environment for this job template.
    - `Playbook` : Select the playbook to be executed by this job.
    - `Credentials`: Select credentials (for nodes, vault, openstack, etc.). You can only select one credential of each type.
    - Varaiables: Pass extra command line variables to the playbook. This is the `-e` or `--extra-vars` command line parameter for ansible-playbook.
    - `Skip Tags`: Skip tags are useful when you have a large playbook, and you want to skip specific parts of a play or task. Use commas to separate multiple tags.
    - Etc.
- The picture below contains everything you need.

![AWX Template 1](./img/awx/awx-template1.PNG)

- Now, hit the Launch button to create a job from this template.

![AWX Template 1](./img/awx/awx-template2.png)

- Click on **`-Views -> Jobs`** and see the Job Template status.

![AWX Job 1](./img/awx/awx-job1.PNG)

![AWX Job 2](./img/awx/awx-job2.PNG)

**Note:** You can launch a Job Template everytime you commit your SCM repository.


### 10. Create Your First Openstack Tenant With AWX
Until now we are working on step by step guide to configure our first Project Job Template on Ansible AWX by using a demo inventory with a single host (`localhost` by default). But, the most crucial part in practice is how to use and configure Inventory from a source control version like Gitlab. In this section we focus on Gitlab Inventory Integarion and AWX Automation as part of Openstack Tenant Creation.

- First, we start by importing the Inventory form Giltab.
- Next, we will create an AWX project to run the Ansible Playbook with Openstack Cloud Module to create a new Tenant with three virtual machines (see the Inventory below).

#### Create Inventory | Gitlab Inventory Source

Now that AWX can fetch our Ansible files from Gitlab (we have already set up Gitlab credentials), let's import the inventory form Gitlab.

- **Step 1:**

    - From Gitlab project, select the `master` branch and click on **Clone** and Copy the SSH URL.

![AWX Gitlab Inventory](./img/awx/awx-inventory0.PNG)


**Important** The inventory file should be located in the root directory of the repository, as well as the `group_vars` and `host_vars` folders. AWX detect all files from these folders. This might not work if they are located elsewhere in the repository.

```
[mt-tenant-test]
localhost ansible_connection=local ansible_python_interpreter='{{ ansible_playbook_python }}'
[all:children]
dc1
dc2
dc3

[dc1]
vla-tenant-t01.mt.lan ansible_python_interpreter=/usr/libexec/platform-python
[dc2]
vla-tenant-t02.mt.lan ansible_python_interpreter=/usr/libexec/platform-python
[dc3]
vla-tenant-t03.mt.lan ansible_python_interpreter=/usr/libexec/platform-python

```

<!--![AWX Gitlab Inventory](./img/awx/awx-inventory00.PNG)-->

- **Step 2:**
    - To use the Inventory kept on Gitlab, grab the `gitlab ssh url`. 
    - On the Ansible AWX tower go to the **Projects** pane from the left side pane and click **Add** button to create a new project.
    - Provide a **name** and **description** to a Project. 
    - Select **organization**. 
    - In the **Source Control Credential Type** choose `Git`. 
    - In the **Type Details** area **Source Control URL** is the git ssh url copied above from gitlab. 
    - Provide **Source Control Credential** to connect Gitlab.
    - Choose **Update Revision on Launch** if required. 
    - Save the project.
  ![AWX Gitlab Inventory](./img/awx/awx-inventory1.PNG)
    - Once project is created, make sure it is connected to Gitlab and synced the data from git project to **Project Base Path** `/var/lib/awx/projects` with **Last Job Status** `Successful` message.
  ![AWX Gitlab Inventory](./img/awx/awx-inventory2.PNG)
    - Go Back to Projects view and verify if the project was created successfully and listed with `Successful` status.
  ![AWX Gitlab Inventory](./img/awx/awx-inventory3.PNG)

- **Step 3:**
    - Now, in the **Inventories** tab, click **Add** button from drop down list, select **Add Inventory** to create an empty inventory. 
    - In the Create new inventory wizard, Provide it a **name**, **description** and **organization**. 
    - In the last Save it.
      ![AWX Gitlab Inventory](./img/awx/awx-inventory4.PNG)
    - Once Inventory is saved and created, click on the **Details** tab, from here go to **Sources** tab and click **Add** button.
      ![AWX Gitlab Inventory](./img/awx/awx-inventory5.PNG)
    - In the **Create** new source wizard, provide it a **name** and **description**. 
    - In the **Source** choose **Sourced from a Project**. Search and select the `Project Name` created earlier. 
    - Next from the **Inventory file** choose  `/ (project root)` (the root folder contains the inventory file to be synced by this source). 
    - Check the box of **Update on launch** (Each time a job runs using this project, update the revision of the project prior to starting the job). 
    - Click **Save** button.
      ![AWX Gitlab Inventory](./img/awx/awx-inventory6.PNG)
    - On the **Details** tab, Here click **Sync** button to download and update hosts files servers and groups information inside inventory on AWX.
      ![AWX Gitlab Inventory](./img/awx/awx-inventory7.PNG)
      ![AWX Gitlab Inventory](./img/awx/awx-inventory8.PNG)
    - Click on the **Back to Sources** and verify if the source was created successfully and listed with `Successful` status.

<br>

- **Step 4:**
    - Once `Sync` is completed, back to **Inventory** and click on the **Hosts** tab and verify inventory updated and has the servers list kept and matching on the Gitlab.
    ![AWX Gitlab Inventory](./img/awx/awx-inventory9.PNG)
    - Another thing can be verified by going to **Groups** tab and check groups are listed.
    ![AWX Gitlab Inventory](./img/awx/awx-inventory10.PNG)

#### Create AWX Project
This section focuses on how to setup AWX project to run AWX Automation jobs as part of Openstack Tenant Creation, using Gitlab as a source of ansible playbooks.

- **Playbook Details**
    - The `tenant.yml` playbook is used to deploy all openstack conponents (virtual machines, bindapi and Ansible roles) in an OPSV2 tenant.
    - This playbook deploy :
        - Tenant
        - quota
        - Rbac policy
        - vRouters admin / prodpriv
        - networks and subnets
        - Security groups
        - Instances
        - Floating IPs
        - create record A Dns
        - ansible roles
      ![AWX Gitlab Openstack Tenant](./img/awx/awx-openstack-tenant0.PNG)
    - Note that the `tenant.yml` is divided into 4 steps :
        - **Step 0**: Create the Openstack Tenant (with Openstack Ansible Role)
        - **Step 1**: Create Security group, Network ports, Centos 8 Instance and Record A BindAPI  (with Openstack Ansible Role)
        - **Step 2**: Install System pre-requisites  (with Centos Ansible Role)
        - **Step 3**: Upgrade all packages (with Centos Ansible Role)

```
        - name: Step 0 - Create Tenant
          hosts: localhost
          gather_facts: true
          tags:
            - openstack
          tasks:
            - name: Call Openstack Role with  Admin Creds
              include_role:
                name: openstack
              tags:
                - project
        - name: Step 1 - Create Security group, Network ports, Centos 8 Instance and Record A BindAPI
          hosts: localhost
          gather_facts: true
          environment:
            OS_CLIENT_CONFIG_FILE: /runner/env/clouds.yaml
          vars_files:
            - './vars/{{openstack_client_tenant}}/secret.yml'
            - './vars/{{openstack_client_tenant}}/openstack_creds.yml'
          tags:
            - openstack
          tasks:
            - name: Copy the Cloud Creds to Ansible Runner
              copy:
                content: "{{ cloud_creds | to_nice_yaml(indent=2) }}"
                dest: /runner/env/clouds.yaml
              tags:
                - sshkey
                - create
            - name: Call Openstack Role with with mt-deployawx.ansible Creds
              include_role:
                name: openstack
              tags:
                - sshkey
                - create 
            - name: Create record A for Openstack VM
              include_role:
                name: bindapi
              tags:
                  - bindapi

        - name : Step 2 - Install System pre-requisites
          hosts: all
          user: centos
          become: true
          gather_facts: true
          roles:
            - role: centos
          tags:
            - prereq

        - name : Step 3 - Upgrade all packages
          hosts: all
          user: centos
          become: true
          gather_facts: true
          tasks:
            - name: Upgrade all packages
              yum:
                name: '*'
                state: latest

            - name: Unconditionally reboot the machine with all defaults
              reboot:

            - name: Wait for VM to respond
              wait_for_connection:
                delay: 10
                timeout: 120
          tags:
            - update
```

**Note 1** The `tenant.yml` playbook needs **2 openstacks users** for Step0 and Step1:

  - `mt-automation.ansible` : openstack administrator used to create tenant (for Step0), quota and rbac policies. **This user will be used to  deploy all Openstack Tenants**.
  - `mt-deployawx.ansible` : openstack user to deploy tenant components (for Step1) like security group, vms, fip and bindapi for DNS. **This user should be dynamic and different for each Openstack Tenant**.

**Note 2** The `tenant.yml` playbook allows you to dynamically deploy for each client an Openstack Ternant and a Cloud User.<br>
For this, we added the `openstack_client_tenant` variable in the  `tenant.yml` playbook. This variable let us to set the name of the tenant dynamically. So, we can use the same playbook to deploy multiple Openstack Tenant and a Cloud User for each one (we will present how do this in the `Create AWX Openstack Credentials` section) 

So To do this, in the `vars` folder of the Gitlab project you need to put a directory per Openstack Tenant which contains all the information about Openstack Cloud User to be used for no-admin tasks. 
        ![AWX  Openstack](./img/awx/awx-openstack-tenant15.PNG)

In this way, the `tenant.yml` playbook will fetch the corresponding values of the Openstack Tenant based on the value of the `openstack_client_tenant` variable (we put this variable as parameter for the AWX Template):


    - name: Step 1 - Create Security group, Network ports, Centos 8 Instance and Record A BindAPI
      hosts: localhost
      gather_facts: true
      environment:
          OS_CLIENT_CONFIG_FILE: /runner/env/clouds.yaml
      vars_files:
            - './vars/{{openstack_client_tenant}}/secret.yml'
            - './vars/{{openstack_client_tenant}}/openstack_creds.yml'
    

- **Step 1:**

    - From Gitlab project, select the `master` branch and click on **Clone** and Copy the SSH URL.

- **Step 2:**
    - On the Ansible AWX tower go to the **Projects** pane from the left side pane and click **Add** button to create a new project.
    - Provide a **name** and **description** to a Project. 
    - Select **organization**. 
    - Select the **Execution Environment** (For ex. the `MT-AWX-EE` creaed previously )
    - In the **Source Control Credential Type** choose `Git`. 
    - In the **Type Details** area **Source Control URL** is the git ssh url copied from gitlab code project. 
    - Provide **Source Control Credential** to connect Gitlab.
    - Choose **Update Revision on Launch** if required. 
    - Save the project.
  ![AWX  Openstack](./img/awx/awx-openstack-tenant1.PNG)
    - Once project is created, make sure it is connected to Gitlab and synced the data from git project to **Project Base Path** `/var/lib/awx/projects` with **Last Job Status** `Successful` message.
  ![AWX  Openstack](./img/awx/awx-openstack-tenant2.PNG)


    

#### Create AWX Openstack Credentials
- Ansible OpenStack cloud module and requires credentials to do anything meaningful, and specifically requires the following information: `auth_url`, `username`, `password`, `domain_name`, and `project_name`. 
- These fields are made available to the playbook via the environmental variable `OS_CLIENT_CONFIG_FILE`, which points to a YAML file written by AWX based on the contents of the cloud credential.

- For this guide, the `tenant.yml` playbook requires the above credentials to work with Ansible OpenStack cloud module:
    - The `admin` credential: 

    ```
      clouds:
        admin:
          auth:
            auth_url: https://iaas.monaco-telecom.mc:13000
            username: "mt-automation.ansible"
            password: xxxxx
            project_name: admin
            domain_id: default
          region_name: "regionOne"
    ```

    - The `mt-awxdeploy.ansible` credential: 

    ```
    clouds:
      mt-awxdeploy.ansible:
        auth:
          auth_url: https://iaas.monaco-telecom.mc:13000
          username: mt-awxdeploy.ansible
          password: xxxx
          project_name: mt-tenant-test
          domain_id: 860492b51bdb4ecd85a1f8745d9a80f4
        region_name: regionOne
        identity_api_version: 3
    ```

<br>

- **Create the `admin` Credentails**
    - Go to **Credentials** tab and click on **Add** button.
    - Provide a **name** and **description** to a credential. 
    - Select **organization**. 
    - Select the **Credential Type**  from list and choose `Openstack`.
    - Provide **Type Details** according to the `admin` informatins listed above (`auth_url`, `username`, `password`, `project_name` and `domain_name`).
    - Verify SSL is not required.
    - Click on **Save** button.
    ![AWX  Openstack](./img/awx/awx-openstack-tenant7.PNG)

- **Create the ` mt-awxdeploy.ansible` User Credentails**
    - According to the content of the  `tenant.yml` playbook  presented above, in the following steps we show all configurations that we have made in order to make dynamic the creation of this User Credential.
    - First, we create `OS_CLIENT_CONFIG_FILE` Environment Variable: <br>

            environment:
                OS_CLIENT_CONFIG_FILE: /runner/env/clouds.yaml

    - Include `openstack_creds` creds from the `openstack_creds.yml` vars file (this file contains the `mt-awxdeploy.ansible` informatins listed listed above : `auth_url`, `username`, `password`, `project_name` and `domain_name`.<br>

            vars_files:
              - './vars/{{openstack_client_tenant}}/secret.yml'
              - './vars/{{openstack_client_tenant}}/openstack_creds.yml'

    - Copy the Cloud Creds to Ansible Runner<br>

            - name: Copy the Cloud Creds to Ansible Runner
                    copy:
                      content: "{{ cloud_creds | to_nice_yaml(indent=2) }}"
                      dest: /runner/env/clouds.yaml


#### Create AWX Vault Pass Credential
- Some variables were encrypted using Ansible Vault, so you have to create a credential for it. 
- Create a new credential with **Type Vault** and set the password.
    ![AWX  Openstack](./img/awx/awx-openstack-tenant3.PNG)

#### Create AWX Template
- Now, we need to  create a set of  **Job Template** to run the `tenant.yml` playbook. Templates in AWX are used to defined the execution context of one or more playbooks.
- As mentioned above in the `Create AWX Project` section, the `tenant.yml` playbook is divided into 4 steps, so we need to create a Workflow Template to run the same playbook with Ansible Roles for multiple times (respectively for `Step0`, `Step 1`, `Step 2` and `Step 3`).
- The Workflow Template is a set of Job Template, so based on `Ansible Tags` we need to create a Job Tempalte for the different steps (see the screenshot below). To put it simply, we are going to create a Job Template only for Step 0, 1 and 2.
    ![AWX  Openstack](./img/awx/awx-openstack-tenant99.PNG)

- **Create the Job Template for ` Step0`**
    - Go on **Templates** and click on **Add** list and choose `Add Job Template`.
    - Provides all informations listed on this screenshot (Inventory, Project, Exenction Environement, Openstack and Vault Credentials, and Job Tags). Be sure to select the correct Openstack Credential (`admin`).
    ![AWX  Openstack](./img/awx/awx-openstack-tenant11.PNG)
- **Create the Job Template for ` Step1`**
    - Go on **Temaplates** and click on **Add** list and choose `Add Job Template`.
    - Provides all informations listed on the screenshot below (Inventory, Project, Exenction Environement, Vault Credentials, Job Tags, and Skip Tags). Here, we don't need to set Openstack Creds because we have already put it into the Git repository of the Ansible Playbook (`vars/mt-tenat-test` folder).
    ![AWX  Openstack](./img/awx/awx-openstack-tenant12.PNG)

- **Create the Job Template for ` Step2`**
    - Go on **Templates** and click on **Add** list and choose `Add Job Template`.
    - Provides all informations listed on this screenshot (Inventory, Project, Exenction Environement, Vault Credentials and Job Tags). Here, we don't need any Openstack Credentials.
    ![AWX  Openstack](./img/awx/awx-openstack-tenant13.PNG)

- **Create the Workflow Job Template**
    - Go on **Templates** and click on **Add** list and choose `Add Workflow Template`.
    - Provide a **name** and **description** to a credential. 
    - Select **organization**, **Inventory** and **Source Control Branch**.
    - Add the `openstack_client_tenant` variable with `mt-tenant-test` value (for the example here).
    ![AWX  Openstack](./img/awx/awx-openstack-tenant14.PNG)
    - Add the 3 previous Job Template to the Workflow as follows (Choose Job Template for **Node Type**):
    ![AWX  Openstack](./img/awx/awx-openstack-tenant9.PNG)

#### Launch AWX Workflow Tempalte

- We now have everything we need to run the `tenant.yml` playbook.
- Go to **Templates** and click on **Launch Template** of the Workflow Template created above:
  ![AWX  Openstack](./img/awx/awx-openstack-tenant20.PNG)
  ![AWX  Openstack](./img/awx/awx-openstack-tenant10.PNG)

#### Check Rancher and Outputs

- **Step0  Create Tenant Job**
    - Go to Rancher Server and check that a new pod has been created by AWX as ansible runner to execute the first Job of the workflow (a pod will be created for each node):
      ![AWX  Openstack](./img/awx/awx-openstack-tenant16.PNG)
    - Execute Shell on the Pod and check ansible env (especially the `OS_CLIENT_CONFIG_FILE` environment variable) :
      ![AWX  Openstack](./img/awx/awx-openstack-tenant19.PNG)

- **Step1  Tenant Components Job**

    - Click on Workflow Node and Check Job outputs:
      ![AWX  Openstack](./img/awx/awx-openstack-tenant17.PNG)
      ![AWX  Openstack](./img/awx/awx-openstack-tenant18.PNG)

#### Launch Again The AWX Workflow Template to Create a new Openstack Tenant
- By using the same AWX Workflow Tempalte presented above, you can create a second Openstack Tenant (called `mt-tenant-foo` for ex.). To do this: 
- you create a new subdirectory in the `vars` folder which contains all informations about the Tenant and the Cloud User.
- Edit the Workflow Template and set the `openstack_client_tenant` variable to `mt-tenant-foo`, Click on Save button.
- Now click on the launch button for running the Tenant Ansible Playbook.
![AWX  Openstack](./img/awx/awx-openstack-tenant21.PNG)

