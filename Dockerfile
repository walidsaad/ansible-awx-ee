ARG EE_BASE_IMAGE=quay.io/ansible/ansible-runner:stable-2.12-devel
ARG EE_BUILDER_IMAGE=quay.io/ansible/ansible-builder:latest

FROM $EE_BASE_IMAGE as galaxy
ARG ANSIBLE_GALAXY_CLI_COLLECTION_OPTS=
USER root

ADD _build/ansible.cfg ~/.ansible.cfg

ADD _build /build
WORKDIR /build

RUN ansible-galaxy role install -r requirements.yml --roles-path /usr/share/ansible/roles
RUN ansible-galaxy collection install $ANSIBLE_GALAXY_CLI_COLLECTION_OPTS -r requirements.yml --collections-path /usr/share/ansible/collections

FROM $EE_BUILDER_IMAGE as builder

COPY --from=galaxy /usr/share/ansible /usr/share/ansible

ADD _build/bindep.txt bindep.txt
RUN ansible-builder introspect --sanitize --user-bindep=bindep.txt --write-bindep=/tmp/src/bindep.txt --write-pip=/tmp/src/requirements.txt
RUN assemble

FROM $EE_BASE_IMAGE
USER root

COPY --from=galaxy /usr/share/ansible /usr/share/ansible

COPY --from=builder /output/ /output/
RUN /output/install-from-bindep && rm -rf /output/wheels
RUN alternatives --set python /usr/bin/python3
COPY --from=quay.io/ansible/receptor:devel /usr/bin/receptor /usr/bin/receptor
RUN mkdir -p /var/run/receptor
ADD run.sh /run.sh
CMD /run.sh
RUN python3 -m venv /var/lib/awx/venv/ansible2_0
RUN python3 -m venv /var/lib/awx/venv/ansible2_4
RUN python3 -m venv /var/lib/awx/venv/ansible2_6
RUN python3 -m venv /var/lib/awx/venv/ansible2_8
RUN python3 -m venv /var/lib/awx/venv/ansible2_9
RUN python3 -m venv /var/lib/awx/venv/ansible2_10
RUN /var/lib/awx/venv/ansible2_9/bin/pip install --upgrade pip
RUN /var/lib/awx/venv/ansible2_9/bin/pip install psutil
RUN /var/lib/awx/venv/ansible2_9/bin/pip install -U "ansible == 2.9"
RUN /var/lib/awx/venv/ansible2_9/bin/pip install -U "python-openstackclient == 5.6.0"
RUN /var/lib/awx/venv/ansible2_9/bin/pip install -U "openstackclient == 4.0.0"
USER 1000
RUN git lfs install
